package com.example.iths.fuckingamazingprojectbro.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.iths.fuckingamazingprojectbro.R;
import com.example.iths.fuckingamazingprojectbro.activity.interfaces.LoginServiceCallback;
import com.example.iths.fuckingamazingprojectbro.data.AbstractEmployee;
import com.example.iths.fuckingamazingprojectbro.service.LoginService;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

/**
 * Class that handles the login
 */
public class LoginActivity extends AppCompatActivity implements LoginServiceCallback {

    private static final String SAVED_ACCOUNT = "SAVED_ACCOUNT";
    private static final String USERNAME = "USERNAME";
    private static final String PASSWORD = "PASSWORD";

    private SharedPreferences sh;
    private boolean hasLoggedIn;
    private EditText username;
    private EditText password;
    private CheckBox cb;
    private boolean[] emptys;

    private LoginService loginService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //Sets up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setLogo(R.drawable.menu_logo);

        sh = getSharedPreferences(SAVED_ACCOUNT, MODE_PRIVATE);

        //Gets username and password input fields from the view
        username = (EditText) findViewById(R.id.edit_text_username);
        password = (EditText) findViewById(R.id.edit_text_password);
        cb = (CheckBox) findViewById(R.id.check_box_remember_me);

        /*
         * If the a saved account is found,
         * set username and password to saved data.
         */

        if(sh.getBoolean(LoginActivity.SAVED_ACCOUNT, false)) {
            username.setText(sh.getString(USERNAME, ""));
            password.setText(sh.getString(PASSWORD, ""));
            cb.setChecked(true);
        }

        this.loginService = new LoginService(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Method for inflating the toolbar
     * @param menu the menu that will be inflated
     * @return returns true by default
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    /**
     * (this is an onClick-listener)
     * This will take in username and password and see if the input is right.
     * If so, login and go to the main-activity.
     * Otherwise output an error message.
     * @param view - the button
     */
    public void loginClick(View view) {
        TextView username = (TextView) findViewById(R.id.edit_text_username);
        TextView password = (TextView) findViewById(R.id.edit_text_password);
        login(username.getText().toString(), password.getText().toString());
    }

    private void login(String username, String password) {
        if(!hasLoggedIn) {
            this.loginService.login(username, password);
            hasLoggedIn = true;
            if (cb.isChecked()) {
                saveAccountToSharedPreferences();
            } else {
                SharedPreferences.Editor edit = sh.edit();
                edit.putBoolean(SAVED_ACCOUNT, false);
                edit.apply();
            }
        }
    }

    @Override
    protected void onStart() {
        this.hasLoggedIn = false;
        BaseActivity.logoutHack = false;

        // If the checkbox is unchecked, set username and password to ""
        if(!cb.isChecked()) {
            username.setText("");
            password.setText("");
        }
        super.onStart();
    }

    /*
     * This will save the username and password in SharedPreferences
     */
    private void saveAccountToSharedPreferences() {
        SharedPreferences.Editor edit = sh.edit();
        edit.putBoolean(SAVED_ACCOUNT, true);
        edit.putString(USERNAME, username.getText().toString());
        edit.putString(PASSWORD, password.getText().toString());
        edit.apply();
    }

    /**
     * Listener function. This method will start the scanner if the user chooses to use the
     * scanner function to login
     * @param view the "scan" button
     */
    public void scanClick(View view) {
        IntentIntegrator scanIntegrator = new IntentIntegrator(this);
        scanIntegrator.initiateScan();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        String[] usernamePassword;
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

        String scanningResultContents = scanningResult.getContents();

        try {
            // Split the result into an array of username and password
            usernamePassword = scanningResultContents.split(" ");

            // Try to login with the username and password
            login(usernamePassword[0], usernamePassword[1]);
        }catch (Exception e) {
           Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /**
     * Method that get called when the user has successfully logged in
     * Sets the logged in employee in LoginService
     * Starts MainMenuActivity
     * @param employee the employee that is returned from the server (LoginService)
     */
    @Override
    public void LoginServiceSuccess(AbstractEmployee employee) {
        LoginService.setCurrentEmployee(employee);
        startActivity(new Intent(this, MainMenuActivity.class));
    }

    /**
     * Method that gets called by LoginService if the user does not log in successfully.
     * Posts a toast that tells the user it was unable to log in
     * @param e the exception that LoginService throws if the user is unable to log in
     */
    @Override
    public void LoginServiceFailed(Exception e) {
        this.hasLoggedIn = false;
        Toast toast = Toast.makeText(
                getApplicationContext(),
                R.string.login_failed,
                Toast.LENGTH_SHORT
        );
        toast.show();
    }
}