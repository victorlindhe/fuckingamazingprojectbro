package com.example.iths.fuckingamazingprojectbro.service;

import android.os.AsyncTask;

import com.example.iths.fuckingamazingprojectbro.activity.interfaces.EmployeeOrdersServiceCallback;
import com.example.iths.fuckingamazingprojectbro.data.Order;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
/**
 * Service class that interacts with the web api on matters pertaining to orders that belong
 * to a specific employee.
 */
public class EmployeeOrdersService {
    private EmployeeOrdersServiceCallback callback;
    private Exception error;

    public static final int FETCHMODE_EXCLUDE_DELIVERED = 1;
    public static final int FETCHMODE_INCLUDE_DELIVERED = 2;
    public static final int FETCHMODE_ONLY_DELIVERED = 3;

    /**
     * Default constructor
     * @param callback - The activity that uses this service
     */
    public EmployeeOrdersService(EmployeeOrdersServiceCallback callback){
        this.callback = callback;
    }

    /**
     * Get orders belonging to specific employee
     * @param employeeId - The id of the employee to get orders for
     * @param numOfOrders - The amount of orders to get
     * @param fetchMode - Include/exclude certain order states
     */
    public void get(int employeeId, int numOfOrders, int fetchMode)
    {
        String url = String.format(
                "http://apic1.udda.net/androidproject/rest/orders?employee_id=%s&limit=%s",
                employeeId, numOfOrders);

        switch(fetchMode){
            case 1:
                break;
            case 2:
                url += "&delivered=true";
                break;
            case 3:
                url += "&delivered=only";
                break;
            default:
                break;
        }

        this.get(url);
    }

    private void get(final String apiUrl) {
        new AsyncTask<String, Void, String>() {
            protected String doInBackground(String... strings) {
                try {
                    //Url to the api
                    URL url = new URL(apiUrl);

                    //Connect to api and get response
                    URLConnection connection = url.openConnection();

                    InputStream is = connection.getInputStream();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                    //Loop throgh response and build a string from it
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    //Returns to onPostExecute
                    return result.toString();

                } catch (Exception e) {
                    //I have no idea what kind of exceptions we can get here so let's just catch all of them
                    error = e;
                }

                //Returns to onPostExecute
                return null;
            }

            protected void onPostExecute(String s) {
                //If something went wrong in doInBackground we catch it here
                if(s == null && error != null){
                    //callback.EmployeeOrdersServiceGetFailed(error);
                    //callback.EmployeeOrdersServiceFailed(error);
                    return;
                }
                try {
                    ArrayList<Order> orders = new ArrayList<>();

                    //Used to check if string from api is JSONObject or JSONArray
                    Object json = new JSONTokener(s).nextValue();

                    //Instantiate a JSONArray differently depending on the response we got from the api
                    JSONArray jsonArray;
                    if (json instanceof JSONObject){
                        jsonArray = new JSONArray();
                        jsonArray.put(json);
                    }
                    else if (json instanceof JSONArray){
                        jsonArray = new JSONArray(s);
                    }
                    else{
                        //Java gets angry if I try to use an object instantiated in an if statement
                        Exception e = new JSONException("This shouldn't happen");
                     //   callback.EmployeeOrdersServiceGetFailed(e);
                  //      callback.EmployeeOrdersServiceFailed(e);
                        return;
                    }

                    //Build orders from the JSONObjects in the JSONArray
                    for(int i = 0; i < jsonArray.length(); i++){
                        Order order = new Order();
                        order.populate(jsonArray.getJSONObject(i));
                        orders.add(order);
                    }

                   callback.EmployeeOrdersServiceGetSuccess(orders);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }
}
