package com.example.iths.fuckingamazingprojectbro.helper;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.example.iths.fuckingamazingprojectbro.R;
import com.example.iths.fuckingamazingprojectbro.data.Order;

/**
 * This adapter manage the view of the MyOrdersList, keeping the checkboxed marked..
 * Collects information that is collected by the MyOrdersActivity...
 */
public class MyCustomAdapter extends BaseAdapter {
    private ArrayList<Order> orders;
    private Context context;

    public MyCustomAdapter(Context context, ArrayList<Order> orders) {
        this.context = context;
        this.orders = orders;
    }

    /**
     * This class is a viewholder view that keeps the checkboxes checked when scrolling the list...
     */
    private class ViewHolder {
        CheckBox checkbox;
    }

    /**
     * @return returns the size of the order arraylist
     */
    @Override
    public int getCount() {
        return orders.size();
    }

    /**
     * Method for getting an order in the list
     * @param position where the order is positioned in the list
     * @return the order in the list (Object)
     */
    @Override
    public Object getItem(int position) {
        return orders.get(position);
    }

    /**
     * @param position the position where the item is in the list
     * @return returns the items position in the list
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.my_orders_list_layout, null);

            holder = new ViewHolder();
            holder.checkbox = (CheckBox) convertView.findViewById(R.id.checkBoxCheckOrder);
            convertView.setTag(holder);

            holder.checkbox.setOnClickListener( new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v ;
                    Order order = (Order) cb.getTag();
                    order.setSelected(cb.isChecked());
                }
            });
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        Order order = orders.get(position);
        holder.checkbox.setChecked(order.isSelected());
        holder.checkbox.setTag(order);
        CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkBoxCheckOrder);
        TextView textViewAddress = (TextView) convertView.findViewById(R.id.textViewAddress);
        textViewAddress.setText(""+orders.get(position).getDeliveryStreetAdress() + "\n" + orders.get(position).getDeliveryZipCode() + " " + orders.get(position).getDeliveryCity());
        checkBox.setText("" + orders.get(position).getId());

        return convertView;
    }

    /**
     *
     * @return the updated arraylist where the selected boolean have been change depending on the checkboxed...
     */
    public ArrayList<Order> returnArrayOfOrders (){
        return orders;
    }

}
