package com.example.iths.fuckingamazingprojectbro.helper;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;

/**
 * This is a location listener that will find the user's current position
 */
public class MyCurrentPositionListener implements LocationListener {
    /**
     *
     * @returns an instance of this class.
     */
    public static MyCurrentPositionListener getInstance() {
        return ourInstance;
    }

    private static MyCurrentPositionListener ourInstance = new MyCurrentPositionListener();
    private LatLng currentLocation;

    private MyCurrentPositionListener() {
        
    }

    /**
     *
     * @returns the users current position as a LatLong.
     */
    public LatLng getCurrentLocation() {
        return currentLocation;
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
