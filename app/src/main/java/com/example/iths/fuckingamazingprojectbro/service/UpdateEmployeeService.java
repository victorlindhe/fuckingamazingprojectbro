package com.example.iths.fuckingamazingprojectbro.service;

import android.os.AsyncTask;

import com.example.iths.fuckingamazingprojectbro.activity.interfaces.UpdateEmployeeServiceCallback;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Service class that interacts with the web api to update an employee
 */
public class UpdateEmployeeService {

    private UpdateEmployeeServiceCallback callback;
    private Exception error;

    public static final String SETTING_ORDERS_IN_BATCH = "orders_in_batch";

    /**
     * Default constructor
     * @param callback - The activity that uses this service
     */
    public UpdateEmployeeService(UpdateEmployeeServiceCallback callback){
        this.callback = callback;
    }

    /**
     * Update a setting
     * @param employeeUsername - The username of the employee to update (this is the employee id atm)
     * @param setting - The name of the setting to update
     * @param value - The value to update with
     */
    public void setting(final String employeeUsername, final String setting, final String value){
        new AsyncTask<String, Void, String>() {
            protected String doInBackground(String... strings) {
                try {
                    //Data to post
                    String charset = "UTF-8";
                    String query = String.format("username=%s&setting=%s&value=%s",
                            URLEncoder.encode(employeeUsername, charset),
                            URLEncoder.encode(setting, charset),
                            URLEncoder.encode(value, charset));

                    //Set connection settings and request properties
                    URL url = new URL("http://apic1.udda.net/androidproject/rest/employees/");
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoOutput(true);
                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                    //Write the post data
                    OutputStream output = connection.getOutputStream();
                    output.write(query.getBytes());

                    int responseCode = connection.getResponseCode();
                    return String.valueOf(responseCode);

                }
                catch(Exception e){
                    error = e;
                    return null;
                }

            }
            protected void onPostExecute(String responseCode){
                callback.UpdateEmployeeServiceSuccess(responseCode);
            }
        }.execute();
    }
}
