package com.example.iths.fuckingamazingprojectbro.service;

import android.os.AsyncTask;

import com.example.iths.fuckingamazingprojectbro.activity.interfaces.UpdateOrderServiceCallback;
import com.example.iths.fuckingamazingprojectbro.data.Order;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Service class that interacts with the web api to update an order
 */
public class UpdateOrderService {
    private UpdateOrderServiceCallback callback;
    private Exception error;

    /**
     * Default constructor
     * @param callback - The activity that uses this service
     */
    public UpdateOrderService(UpdateOrderServiceCallback callback){
        this.callback = callback;
    }

    /**
     * Update an order to be delivered
     * Don't ask me why we chose to couple this service with the order object. IDK!
     * @param order - An order object with deliveredAt, deliveryLatitude, deliveryLongitude set
     */
    public void deliver(final Order order){
        new AsyncTask<String, Void, String>() {
            protected String doInBackground(String... strings) {
                try {
                    String charset = "UTF-8";

                    //Set post data
                    String query = String.format("id=%s&delivered_at=%s&delivery_latitude=%s&delivery_longitude=%s",
                            URLEncoder.encode(String.valueOf(order.getId()), charset),
                            URLEncoder.encode(order.getDeliveredAt(), charset),
                            URLEncoder.encode(order.getDeliveryLatitude(), charset),
                            URLEncoder.encode(order.getDeliveryLongitude(), charset));

                    URL url = new URL("http://apic1.udda.net/androidproject/rest/orders/");

                    //Set connection settings and request properties
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoOutput(true);
                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                    //Write the post data
                    OutputStream output = connection.getOutputStream();
                    output.write(query.getBytes());

                    //This implicitly fires the connection while also giving us a response
                    //I have no intentions of wasting my time by sending the correct response codes from the api
                    //200 is aaaaaaaall we need
                    int responseCode = connection.getResponseCode();

                    //Returns to onPostExecute
                    return String.valueOf(responseCode);

                } catch (Exception e) {
                    //I have no idea what kind of exceptions we can get here so let's just catch all of them
                    error = e;
                }

                //Returns to onPostExecute
                return null;
            }

            protected void onPostExecute(String s) {
                //If something went wrong in doInBackground we catch it here
                //This is untested error handling. We'll see if we can trigger some exceptions to try it out
                if(Integer.parseInt(s) == 0  && error != null)
                    callback.UpdateOrderServiceFailed(error);
                else if(Integer.parseInt(s) == 200)
                    callback.UpdateOrderServiceSuccess(s);
            }
        }.execute();
    }
}
