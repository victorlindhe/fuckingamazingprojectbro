package com.example.iths.fuckingamazingprojectbro.activity.interfaces;

import com.example.iths.fuckingamazingprojectbro.data.AbstractEmployee;

/**
 * Created by iths on 2015-12-07.
 */
public interface LoginServiceCallback {
    public void LoginServiceFailed(Exception e);
    public void LoginServiceSuccess(AbstractEmployee employee);
}
