package com.example.iths.fuckingamazingprojectbro.data;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Class that represents a Driver
 */

public class Driver extends AbstractEmployee {
    private static final String FIRST_NAME_KEY = "first_name";
    private static final String LAST_NAME_KEY = "last_name";
    private static final String PASSWORD_KEY = "password";

    private ArrayList<Order> orderArrayList;

    /**
     * Constructor for Driver, uses the superclasses constructor
     */

    public Driver(){
        super();
    }


    /**
     * Method for returning the drivers orders
     * @return the drivers orders
     */
    @Override
    public ArrayList<Order> getOrders() {
        return orderArrayList;
    }

    /**
     * Populates the driver with data from the database, sets the drivers name, last name, password
     * and ID. Also fetches the settings from the database. 
     * @param data the data from the database
     */
    @Override
    public void populate(JSONObject data) {
        this.setId(data.optInt("id"));
        this.setFirstName(data.optString(FIRST_NAME_KEY));
        this.setLastName(data.optString(LAST_NAME_KEY));
        this.setPassWord(data.optString(PASSWORD_KEY));

        JSONArray settings = data.optJSONArray("settings");
        for(int i = 0; i < settings.length(); i++)
        {
            JSONObject setting = null;
            try {
                setting = settings.getJSONObject(i);
            } catch (JSONException e) {
                
            }

            this.setSetting(setting.optString("name"), setting.optString("value"));
        }
    }
}
