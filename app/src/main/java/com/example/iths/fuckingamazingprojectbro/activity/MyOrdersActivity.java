package com.example.iths.fuckingamazingprojectbro.activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.example.iths.fuckingamazingprojectbro.activity.interfaces.EmployeeOrdersServiceCallback;
import com.example.iths.fuckingamazingprojectbro.activity.interfaces.UpdateOrderServiceCallback;
import com.example.iths.fuckingamazingprojectbro.data.AbstractEmployee;
import com.example.iths.fuckingamazingprojectbro.helper.MyCurrentPositionListener;
import com.example.iths.fuckingamazingprojectbro.data.Order;
import com.example.iths.fuckingamazingprojectbro.R;
import com.example.iths.fuckingamazingprojectbro.helper.MyCustomAdapter;
import com.example.iths.fuckingamazingprojectbro.service.EmployeeOrdersService;
import com.example.iths.fuckingamazingprojectbro.service.LoginService;
import com.example.iths.fuckingamazingprojectbro.service.UpdateEmployeeService;
import com.example.iths.fuckingamazingprojectbro.service.UpdateOrderService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Calendar;

/**
 * This class displays the specific orders to be delivered for the corresponding driver.
 * This class enable the driver to deliver orders and collect new orders.
 */
public class MyOrdersActivity extends BaseActivity implements UpdateOrderServiceCallback, EmployeeOrdersServiceCallback {
    private static final String KEY_LISTSIZE = "sizeOfArrayList2SaveForLaterUse";
    private static final String KEY_SHARED = "key4SharedPreferences";

    private int lastArrayListSiZe;
    private boolean sharedInOnCreate = false;

    private UpdateOrderService updateOrderService;
    private EmployeeOrdersService employeeOrdersService;

    private ArrayList<Order> orders; //The array we populate our listview from
    private MyCustomAdapter customAdapter;
    private ListView MyOrdersListViewOrders;

    private Vibrator vibrator;
    private MyCurrentPositionListener currentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);

        //Sets up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setLogo(R.drawable.menu_logo);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        vibrator = (Vibrator) getSystemService(this.VIBRATOR_SERVICE);
        currentLocation = MyCurrentPositionListener.getInstance();

        this.orders = new ArrayList<Order>();
        manageSharedPreferences();
        sharedInOnCreate = true;


        if (lastArrayListSiZe == 0) {
            AbstractEmployee loggedInEmployee = LoginService.getCurrentEmployee();
            lastArrayListSiZe = Integer.parseInt(loggedInEmployee.getSetting(
                    UpdateEmployeeService.SETTING_ORDERS_IN_BATCH));
        }

        this.updateOrderService = new UpdateOrderService(this);
        this.employeeOrdersService = new EmployeeOrdersService(this);

        this.employeeOrdersService.get(LoginService.getCurrentEmployee().getId(),
                lastArrayListSiZe, employeeOrdersService.FETCHMODE_EXCLUDE_DELIVERED);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        // Go back to the main activity
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        // Handles the navigation button
        if (id == R.id.next_order_navigation) {
            if (orders.size() != 0) {
                final Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("google.navigation:q=" + orders.get(0).getFullOrderAdress()));
                //Creates the dialog where the user is prompted to send an sms to the customer or not
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.deliver_to_customer_sms_message);
                builder.setPositiveButton(R.string.positive_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK, the customer gets sent an SMS and the GPS navigation starts

                        SmsManager smsManager = SmsManager.getDefault();

                        smsManager.sendTextMessage(
                                orders.get(0).getDeliveryPhoneNumber(),
                                null,
                                getResources().getString(R.string.delivery_on_its_way_message),
                                null,
                                null
                        );

                        startActivity(intent);
                    }
                });

                builder.setNegativeButton(R.string.negative_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog and the GPS navigation starts
                        startActivity(intent);
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
                return true;

            } else {
                Toast.makeText(this, getString(R.string.no_order_available), Toast.LENGTH_SHORT).show();
                vibrator.vibrate(750);
            }
        }

        // Handles the update button which updates the orders in the list
        if (id == R.id.update) {
            AbstractEmployee loggedInUser = LoginService.getCurrentEmployee();
            int amountOfOrders = Integer.parseInt(loggedInUser.getSetting(
                    UpdateEmployeeService.SETTING_ORDERS_IN_BATCH));
            this.employeeOrdersService.get(
                    loggedInUser.getId(), amountOfOrders, employeeOrdersService.FETCHMODE_EXCLUDE_DELIVERED);

        }

        //Logs out the user
        if(id == R.id.action_logout) {
           BaseActivity.logout(this);
        }

        return super.onOptionsItemSelected(item);
    }

     private void deliverOrders() {
         this.orders = customAdapter.returnArrayOfOrders();
         ArrayList<Order> deliveredOrders = new ArrayList<>();
         ArrayList<Order> ordersTemp = new ArrayList<>();

         if (checkIfOrdersSelected(orders)) {
             //Check all orders for selected ones
             for (int i=0; i<orders.size();i++) {
                 if (!orders.get(i).isSelected()) {
                     //If the order is not selected for delivery we store it temporary
                     //and then add it back to the list after we've delivered the selected orders
                     ordersTemp.add(orders.get(i));
                 } else {
                     //If the order was selected for delivery we set some fields and add it to the another list and later deliver it
                     String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
                     orders.get(i).setDeliveredAt(timestamp);
                     if (currentLocation.getCurrentLocation()!=null) {
                         orders.get(i).setDeliveryLatitude(String.valueOf(currentLocation.getCurrentLocation().latitude));
                         orders.get(i).setDeliveryLongitude(String.valueOf(currentLocation.getCurrentLocation().longitude));
                     }
                     deliveredOrders.add(orders.get(i));
                 }
             }

             //Deliver the orders
             for(Order order : deliveredOrders){
                 this.updateOrderService.deliver(order);
             }

             //Clear the list of all orders and then repopulate it with only the non selected orders
             this.orders.clear();
             for (int i=0; i<ordersTemp.size();i++) {
                 orders.add(ordersTemp.get(i));
             }
             customAdapter.notifyDataSetChanged();
         } else {
             vibrator.vibrate(750);
             Toast.makeText(MyOrdersActivity.this, R.string.No_order_was_checked, Toast.LENGTH_SHORT).show();
         }
    }

    // Method the checking if any of the orders in the list is selected
    private boolean checkIfOrdersSelected(ArrayList<Order> orders) {
        int moreThanZero = 0;
        for (int i=0; i<orders.size(); i++){
            if (orders.get(i).isSelected()) {
                moreThanZero++;
            }
        }
        if (moreThanZero > 0){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_orders, menu);
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences sharedPreferences = getSharedPreferences(KEY_SHARED, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt(KEY_LISTSIZE, orders.size());

        editor.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

        manageSharedPreferences();
        sharedInOnCreate = false;
    }

    private void manageSharedPreferences() {
        if (!sharedInOnCreate){
            SharedPreferences sharedPreferences = getSharedPreferences(KEY_SHARED, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            //TODO ändra chosenBatchSize?
            int numOfOrders = Integer.parseInt(LoginService.getCurrentEmployee().getSetting(
                    UpdateEmployeeService.SETTING_ORDERS_IN_BATCH));
            lastArrayListSiZe = sharedPreferences.getInt(KEY_LISTSIZE, numOfOrders);

            editor.remove(KEY_LISTSIZE);
            editor.commit();
        }
    }

    @Override
    public void UpdateOrderServiceFailed(Exception e) {

    }

    @Override
    public void UpdateOrderServiceSuccess(String message) {
        Toast.makeText(this, getResources().getString(R.string.delivered),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void EmployeeOrdersServiceGetFailed(Exception e) {

    }

    @Override
    public void EmployeeOrdersServiceGetSuccess(ArrayList<Order> returnOrders) {
        this.orders = returnOrders;

        //Sorting array by 1.Zip 2.Address 3.Id
        Collections.sort(orders, Order.orderIdComparator);
        Collections.sort(orders, Order.orderAddressComparator);
        Collections.sort(orders, Order.orderZipComparator);

        MyOrdersListViewOrders = (ListView) findViewById(R.id.MyOrdersListViewOrders);
        customAdapter = new MyCustomAdapter(this, orders);
        MyOrdersListViewOrders.setAdapter(customAdapter);
        MyOrdersListViewOrders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("order", orders.get(position));
                Intent intent = new Intent(MyOrdersActivity.this, OrderDetailsActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);  //This intent takes you to orderDetailsActivity and adds corresponding order as serializable...
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deliverOrders();    //Delivers the checkboxed orders...
            }
        });
    }
}
