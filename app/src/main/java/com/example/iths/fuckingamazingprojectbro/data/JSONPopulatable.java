package com.example.iths.fuckingamazingprojectbro.data;

import org.json.JSONObject;

/**
 * Created by iths on 2015-11-20.
 */
public interface JSONPopulatable {
    public void populate(JSONObject data);
}
