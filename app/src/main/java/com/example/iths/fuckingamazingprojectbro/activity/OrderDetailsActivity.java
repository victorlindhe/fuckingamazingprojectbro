package com.example.iths.fuckingamazingprojectbro.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.iths.fuckingamazingprojectbro.R;
import com.example.iths.fuckingamazingprojectbro.data.Order;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class OrderDetailsActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Order order;
    private TextView customerText;
    private TextView orderText;
    private TextView orderInfo;
    private TextView isDeliveredText;
    private TextView gpsPositionText;
    private TextView orderID;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        setUpMapIfNeeded();

        // Sets up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.drawable.menu_logo);
        toolbar.inflateMenu(R.menu.generic_menu);

        // Gets all the textviews in the layout
        customerText = (TextView) findViewById(R.id.order_details_customer_text);
        orderID = (TextView) findViewById(R.id.order_details_order_id);
        orderText = (TextView) findViewById(R.id.order_details_order_text);
        orderInfo = (TextView) findViewById(R.id.order_details_order_info);
        isDeliveredText = (TextView) findViewById(R.id.order_details_is_delivered_text);

        // Receives the intent from MyOrdersActivity. Gets all the information about the order through the intent
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        order = (Order) bundle.getSerializable("order");

        // Writes out all the information about the order in the different text views
        orderID.setText(Integer.toString(order.getId()));
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(order.getDeliveryEmailAdress()
                + "\n" + order.getDeliveryPhoneNumber()
                + "\n" + order.getDeliveryStreetAdress()
                + "\n" + order.getDeliveryZipCode() + " " + order.getDeliveryCity());
        orderInfo.setText(stringBuilder.toString());

        try {
            String deliveryTimeStamp = order.getDeliveredAt();
            if(deliveryTimeStamp == null || deliveryTimeStamp.equals("null")){
                isDeliveredText.setVisibility(View.GONE);
            }
            else{
                isDeliveredText.append(" " + deliveryTimeStamp.toString());

            }
        } catch (Exception e) {
        }

        try {
            String gpsCoordinates = order.getDeliveredAt();
            gpsPositionText.setText(gpsCoordinates.toString());
        } catch (Exception e) {
        }

        setUpMap();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home){
            NavUtils.navigateUpFromSameTask(this);
        }

        // Quit this activity and log out
        if (id == R.id.action_logout) {
            BaseActivity.logout(this);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.generic_menu, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     */

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }


    /**
     * This is where the map is setup. For example we can here change whether the maps should
     * open in Siberia och Sweden. Right now it uses the coordinates from the order. 
     *
     * This should only be called once and when we are sure that mMap is not null.
     */
    private void setUpMap() {
        if (order != null) {
            try {

                double latitude = Double.parseDouble(order.getDeliveryLatitude());
                double longitude = Double.parseDouble(order.getDeliveryLongitude());

                LatLng position = new LatLng(latitude, longitude);
                mMap.addMarker(new MarkerOptions().position(position)
                        .title(getResources().getString(R.string.order_number) + order.getId()));

                CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(new CameraPosition(position, 15, 0, 0));
                mMap.moveCamera(cameraUpdate);
            } catch (Exception e) {

                View f = (View) findViewById(R.id.map);
                f.setVisibility(View.GONE);
            }
        }
    }
}
