package com.example.iths.fuckingamazingprojectbro.helper;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.iths.fuckingamazingprojectbro.R;
import com.example.iths.fuckingamazingprojectbro.data.Order;
import java.util.ArrayList;

/**
 * This adapter manage the view of the DeliveredOrdersList...
 */
public class MyCustomAdapterDeliveredOrders extends BaseAdapter {
    private final LayoutInflater inflater;
    private ArrayList<Order> deliveredOrders;
    private TextView textViewOrderNumbers;
    private TextView textViewDeliveryDate;
    private TextView textViewAddressInDeliveredOrders;

    /**
     * @param activity - the activity.
     * @param deliveredOrders - An array-list of delivered orders.
     */
    public MyCustomAdapterDeliveredOrders(Activity activity, ArrayList<Order> deliveredOrders) {
        this.inflater = activity.getLayoutInflater();
        this.deliveredOrders = deliveredOrders;
    }

    @Override
    public int getCount() {
        return deliveredOrders.size();
    }

    @Override
    public Object getItem(int position) {
        return deliveredOrders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       View view;
        if (convertView == null) {
            view = inflater.inflate(R.layout.delivered_orders_list_layout, parent, false);
        } else {
            view = convertView;
        }

        textViewOrderNumbers = (TextView) view.findViewById(R.id.textViewOrderNumbers);
        textViewDeliveryDate = (TextView) view.findViewById(R.id.textViewDeliveryDate);
        textViewAddressInDeliveredOrders = (TextView) view.findViewById(R.id.textViewAddressInDeliveredOrders);
        textViewOrderNumbers.setText(Integer.toString(deliveredOrders.get(position).getId()));
        textViewDeliveryDate.setText(deliveredOrders.get(position).getDeliveredAt());
        textViewAddressInDeliveredOrders.setText(""+deliveredOrders.get(position).getDeliveryStreetAdress() + "\n" + deliveredOrders.get(position).getDeliveryZipCode() + " " + deliveredOrders.get(position).getDeliveryCity());
        return view;
    }
}

