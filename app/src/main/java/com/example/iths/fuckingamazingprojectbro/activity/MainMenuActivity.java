package com.example.iths.fuckingamazingprojectbro.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.iths.fuckingamazingprojectbro.helper.MyCurrentPositionListener;
import com.example.iths.fuckingamazingprojectbro.R;
import com.example.iths.fuckingamazingprojectbro.service.LoginService;

/**
 * This class will be the main-menu in the app,
 * and will be shown directly after the user has logged in.
 */
public class MainMenuActivity extends BaseActivity {

    private LocationManager locationManager;
    private MyCurrentPositionListener currentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        //Sets up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setLogo(R.drawable.menu_logo);

        //Gets the current position from the GPS for later use
        currentPosition = MyCurrentPositionListener.getInstance();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        // Check for permissions
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates("gps", 5000, 0, currentPosition);

        } else {
            locationManager.requestLocationUpdates("gps", 5000, 0, currentPosition);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // Quit this activity and log out
        if (id == R.id.action_logout) {
            BaseActivity.logout(this);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            locationManager.removeUpdates(currentPosition);

        } else {
            locationManager.removeUpdates(currentPosition);
        }
    }

    /**
     * (this is an onClick-listener)
     * This will send an intent and start the MyOrdersActivity
     * @param view
     */
    public void myOrdersClick(View view) {
        startActivity(new Intent(this, MyOrdersActivity.class));
    }

    /**
     * (this is an onClick-listener)
     * This will send an intent start the DeliveredOrdersAcitivty
     * @param view
     */
    public void deliveredOrderClick(View view) {
        startActivity(new Intent(this, DeliveredOrdersActivity.class));
    }

    /**
     * (this is an onClick-listener)
     * This will send an intent and start the SettingsActivity
     * @param view
     */
    public void settingsClick(View view) {
        startActivity(new Intent(this, SettingsActivity.class));
    }
}
